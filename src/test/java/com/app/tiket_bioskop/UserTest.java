package com.app.tiket_bioskop;

import com.app.tiket_bioskop.controller.UserController;
import com.app.tiket_bioskop.dto.AddUserDto;
import com.app.tiket_bioskop.dto.UpdateUserDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserTest {

    @Autowired
    private UserController userController;

    @Test
    @DisplayName("6. Menambahkan user baru")
    void addUser() {
        AddUserDto addUserDto = new AddUserDto("OrangNo6", "OrangNo6@gmail.com", "PasswordNo6");
        userController.addUser(addUserDto);
    }

    @Test
    @DisplayName("7. Mengupdate data User")
    void updateUser() {
        UpdateUserDto updateUserDto = new UpdateUserDto("OrangNo6", "OrangNo6@gmail.com", "PasswordNo6", 1);
        userController.updateUser(updateUserDto);
    }

    @Test
    @DisplayName("8. Menghapus user")
    void deleteUser() {
        userController.deleteUser(1);
    }


}
