package com.app.tiket_bioskop.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity // Mendeklarasikan sebagai kelas Entity
@Table(name = "users") // Membuat nama Tabel
public class Users {

    @Id // primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) // auto_increament
    @Column(name = "user_id")
    private Integer userId;
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;

}
