package com.app.tiket_bioskop.repository;

import com.app.tiket_bioskop.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository // Menandakan sebagai kelas Repository
@Transactional // Untuk pengoperasian database
// pengoperasian DB, Select, Save, Update, Delete
// T : Table, ID : tipe data primary key di table
public interface UserRepository extends JpaRepository<Users, Long> {

    // Menambahkan user
    @Modifying
    @Query(nativeQuery = true, value = "insert into users(username, email, password) values(:username, :email, :password)")
    void addUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password
    );

    // Mengupdate user
    @Modifying
    @Query(nativeQuery = true, value = "update users u set u.username= :username, u.email= :email, u.password= :password where u.user_id= :user_id")
    void updateUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password,
            @Param("user_id") Integer userId
    );

    // Menghapus user
    @Modifying
    @Query(nativeQuery = true, value = "delete from users where user_id= :user_id")
    void deleteUser(
            @Param("user_id") Integer userId
    );

    //FOR JASPER
    @Query(nativeQuery = true, value = "select * from users where user_id = :user_id")
    Users getUserById(
            @Param("user_id") Integer userId
    );


}
