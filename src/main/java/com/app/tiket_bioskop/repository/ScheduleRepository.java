package com.app.tiket_bioskop.repository;

import com.app.tiket_bioskop.entity.Schedules;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ScheduleRepository {

    @Modifying
    @Query(nativeQuery = true, value = "select * from schedules where film_id = :film_id")
    List<Schedules> showScheduleFilm(
            @Param("film_id") Integer filmId
    );

}
