package com.app.tiket_bioskop.dto;

import lombok.Getter;

@Getter
public class AddFilmDto {

    private String filmCode;
    private String filmName;
    private Integer sedangTayang;

    public AddFilmDto() {}

    public AddFilmDto(String filmCode, String filmName, Integer sedangTayang) {
        this.filmCode = filmCode;
        this.filmName = filmName;
        this.sedangTayang = sedangTayang;
    }
}
