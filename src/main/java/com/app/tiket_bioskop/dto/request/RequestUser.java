package com.app.tiket_bioskop.dto.request;

import com.app.tiket_bioskop.dto.AddUserDto;
import com.app.tiket_bioskop.dto.UpdateUserDto;
import com.app.tiket_bioskop.entity.Users;
import org.springframework.stereotype.Component;

@Component
public class RequestUser {

    public Users Add(AddUserDto addUserDto) {
        Users users = new Users();
        users.setUsername(addUserDto.getUsername());
        users.setEmail(addUserDto.getEmail());
        users.setPassword(addUserDto.getPassword());
        return users;
    }

    public Users Update(UpdateUserDto updateUserDto) {
        Users users = new Users();
        users.setUsername(updateUserDto.getUsername());
        users.setEmail(updateUserDto.getEmail());
        users.setPassword(updateUserDto.getPassword());
        users.setUserId(updateUserDto.getUserId());
        return users;
    }
}
