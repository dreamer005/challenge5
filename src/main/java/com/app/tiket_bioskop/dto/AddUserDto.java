package com.app.tiket_bioskop.dto;

import lombok.Getter;

@Getter
public class AddUserDto {

    private String username;
    private String email;
    private String password;

    public AddUserDto() {}

    public AddUserDto(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
